FROM ubuntu
MAINTAINER juancarlos
RUN apt update && apt upgrade
RUN apt install nodejs -y
RUN mkdir -p /var/www/src/
ADD ./server.js /var/www/src/
EXPOSE 8888
WORKDIR /var/www/src/
RUN apt install npm -y
RUN npm install request
CMD ["sh", "-c", "node server.js"]
